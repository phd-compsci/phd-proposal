%------------------
% DOCUMENT TYPE
%------------------

\documentclass[letterpaper,11pt,oneside,dvipsnames,usenames]{article}

%------------------
%PACKAGES
%------------------

\usepackage[left=2cm, right=2cm, bottom=2cm, top=2cm]{geometry} % Margins.
\usepackage[utf8]{inputenc} % Latin characters.
\usepackage[T1]{fontenc} % So the document has copiable accents.
\usepackage{lmodern} % Solves some visualitation issues after using fontenc.
\usepackage{exscale} % Improves tiny operators when font is enlarged with lmodern.
\usepackage{microtype} % Improves spacing between letters. Merely esthetic; Deactivate if errors appear.
\usepackage{graphics} % To insert graphs. They must be in the same folder than the .TEX file.
\usepackage{amsthm, amssymb, amsfonts, latexsym, mathtools,thmtools} % Enviroments and several math symbols.
\usepackage[bookmarksnumbered,colorlinks,linkcolor=Maroon,citecolor=MidnightBlue,urlcolor=MidnightBlue]{hyperref} % References. Colors were changed.
%\usepackage[all]{xy} % To elaborate commutative diagrams.
%\usepackage{setspace} % To manage spacing between lines.
%\usepackage{sectsty} % Changes titles color.
%\usepackage{xpatch} % Changes color in references.
%\usepackage{footmisc} % Customizes footnotes.
\usepackage{tikzsymbols} % Emoticons.
%\usepackage{xpatch} % Patches some other packages.
\usepackage{enumitem} % Customizes enumerations.
\usepackage{multicol} % Multiple columns.


%------------------
% SYMBOLS, RENAMING AND OPERATORS
%------------------

\newcommand{\Comp}{\mathbb{C}} % Complex.
\newcommand{\R}{\mathbb{R}} % Real.
\newcommand{\Q}{\mathbb{Q}} % Rational.
\newcommand{\N}{\mathbb{N}} % Natural.
\newcommand{\Z}{\mathbb{Z}} % Integer.
\newcommand{\K}{\mathbb{K}} % Arbitrary field.
%------------------
\def\max{\mathop{\mbox{\rm máx}}} % Spanish Máximo.
\def\min{\mathop{\mbox{\rm mín}}} % Spanish Mínimo.
\def\inf{\mathop{\mbox{\rm ínf}}} % Spanish Ínfimo.
\def\sin{\mathop{\mbox{\rm sen}}} % Spanish Seno.
\def\arcsin{\mathop{\mbox{\rm arcsen}}} % Spanish Arcoseno.
%------------------
\DeclareMathOperator{\Img}{Im} % Image.
\DeclareMathOperator{\Hom}{Hom} % Hom.
\DeclareMathOperator{\tr}{tr} % Trace.
\DeclareMathOperator{\adj}{adj} % adjoint.
\DeclareMathOperator{\A}{Area} % Area.
\DeclareMathOperator{\Span}{span} % Span.
\DeclareMathOperator{\Ran}{ran} % Rank.
\DeclareMathOperator{\Dom}{Dom} % Domain.
\DeclareMathOperator{\sech}{sech} % sech.
\DeclareMathOperator{\csch}{csch} % csch.
%------------------
\newcommand{\ol}[1]{\overline{#1}} % Short for \overline.
%------------------
\newcommand{\horrule}[1]{\rule{\linewidth}{#1}} % Creates a horizontal rule with width argument.

%------------------
%GENERAL SETTINGS
%------------------
\title{%
	PhD Study Plan\\
	\large Continuous-time Models for Machine Learning}
\author{%
	PhD Student: Alexander Leguizamon-Robayo (\href{mailto:alexanderlr@cs.auu.dk}{alexanderlr@cs.aau.dk})\\
Supervisor: Max Tschaikowski (\href{mailto:tschaikowski@cs.aau.dk}{tschaikowski@cs.aau.dk})  }

%------------------
% BEGIN OF THE DOCUMENT
%------------------

\begin{document}

%------------------
% MAIN TITLE
%------------------

\maketitle
%------------------
% BODY OF THE DOCUMENT
%------------------
\section{Project Summary}
Dynamical systems are mathematical models that study the evolution of certain variables in time. 
These models are extensively used in many areas in science and engineering, from the study of motion, to the study of metabolism in living organisms \cite{la_torre_optimal_2015, ovchinnikov_clue_2021, schweiger_synthesis_1999, boccara_modeling_2010}.
A dynamical system helps to explain a real-life phenomenon, as well as to make predictions about it.
Moreover, it is cheaper to study a dynamical system than to carry out experiments on a real-life system.
These predictions can also be used to find parameters that lead to desired states, in other words to control the system.

On one hand, modern systems are increasingly complex.
They require large numbers of model parameters to describe a real-life phenomenon with enough detail to obtain useful results. 
As models are solved numerically using computers, the increased amount of detail means that a large amount of computational resources is required to reach a solution.
A way to tackle this problem is to find a smaller representation of the system such that its solution preserves important properties of the original system.
This is known as \textit{reduction}.
On the other hand, there is the problem of finding the best model parameters, known as the \textit{optimal control problem}.
An example of this problem is adjusting the amount of thrust of a satellite so that it reaches a desired trajectory using the least amount of fuel.

To define an optimal control strategy, one starts with a mathematical model of the system. 
Then one extends it to account for an appropriate cost function, in order to determine the best or optimal combination of parameters.
With this, one solves this extended system to find a control strategy that is minimizes this defined cost. 
In order to optimally control a complex system the amount of variables can be so large that its solution could be computationally unfeasible.
A natural question to ask is whether to reduce first and then optimize or to optimize first and then reduce.
The answer to this question depends on the reduction algorithm. 
If the reduction algorithm preserves optimality, then it makes sense to always reduce first. 

Our goal is to find conditions to ensure that a reduction algorithm preserves optimality and to propose new reduction algorithms that preserve optimality.
This PhD study will apply methods from differential geometry, abstract algebra, control theory, and computer science in order to build reduction algorithms that preserve optimality.
Along this project, we will find methods to check whether a given reduction algorithm preserves optimality. 
An integral part of this project is to ensure the applicability of its results. 
Therefore, this work will be employed to study models of biochemical and cyber-physical systems.

\newpage
\section{Scientific content of the PhD project}
\subsection{Background}
This project relies on results from many different disciplines. 
We can summarize it in four areas: 
\begin{itemize}
	\item \textbf{Mathematical modeling}:
		many physical, mechanical, biological and artificial proceses can be described by dynamical systems which in turn can be used for simulation and control.
The work of building a mathematical model based on physical assumptions is a nontrivial task. 
A primer on the construction of these models can be found in \cite{boccara_modeling_2010} .
	\item \textbf{Dynamical systems and optimal control}:
modern models rest on a strong mathematical foundation.
In this project we will focus on the study of dynamical systems \cite{katok_introduction_1995} , and its optimal control using Pontriagyn's principle \cite{liberzon_calculus_2012}.
	\item \textbf{Algorithms and verification:}
	in order to develop efficient algorithms a strong foundation on theoretical computer science is required.
To do so, we will rely on results from the areas of algorithmic complexity \cite{cormen_introduction_2009,sipser_introduction_2012} and formal verification \cite{huth_logic_2004,aceto_reactive_2007}.
Regarding the numerical solution of these models, it is necessary to have foundation in the area of numerical analysis where the standard reference is \cite{kincaid_numerical_2002}.
\item \textbf{Other mathematical tools:}
it should be noted that there are many reduction procedures \cite{ohsawa_symmetry_2013, pappas_consistent_2002} that are written using the language of modern differential geometry \cite{lee_introduction_2012, marsden_foundations_1987}.
\end{itemize}

\subsection{State-of-the-art}

Given a dynamical system, \textit{model reduction methods} are a class of methods that builds a new dynamical system with fewer parameters while retaining dynamical features important to the modeler. 
A first family of reduction methods uses truncation techniques to lower the dimensionality of the model while having small approximation errors \cite{antoulas_approximation_2005, tabuada_approximate_2008} .
Although these methods can significantly speed up computation times for numerical simulations, they do so at the expense of inducing approximation errors and a loss of  physical interpretability of the model \cite{ovchinnikov_clue_2021}.
The latter being of great importance to applications such as systems biology. 
Another family of reduction methods is that of \textit{time-scale separation}.
Considering that in complex dynamical systems there are dynamics happening in different time scales, these methods assume that "fast" variables already reached their steady-state and  can therefore be considered as constants.
This leads to a reduced model with only "slow" variables. 
Some examples of these kind of methods are \cite{okino_simplification_1998, segel_quasi-steady-state_1989}.
A third family of reduction methods is that of \textit{exact model reduction}.
In this case, the aim is to exploit symmetries of the system in order to reduce it without inducing approximation errors  \cite{tabuada_approximate_2008}. 
These methods work by using model symmetries to induce invariants that lead to reductions that preserve the structure of the dynamics.
In general,  exact model reduction methods need not preserve physical interpretability of the model. 
However, it is possible to find interpretable sets of invariants which preserve some physical interpretability in the reduced model. 
An approach to exact model reduction is that of conservation analysis which detects a linear combination of variables that are constant throughout the evolution of the system.
Another approach is that of exact lumping in which the reduced system is written in terms of macro-variables which are a linear combination of the original variables.
Some examples of exact reduction methods are \cite{li_general_1989,li_general_1994, ovchinnikov_clue_2021, pappas_consistent_2002, cardelli_maximal_2017, cardelli_exact_2021}. 

It should be noted that all these methods are not independent and can be used in tandem. 
For example, one could apply lumping and then time-scale separation to reduce a system. 
Moreover, it is possible to split the state-space of the system and apply a different reduction method to each part \cite{ovchinnikov_clue_2021}. 

As mentioned earlier, the goal is to use these models for simulation and control.
Our focus lies particularly in the study of optimal control, that is control that optimizes a given cost throughout the evolution of the system. 
A common tool for this is Pontriagyn's Maximum Principle (PMP) \cite{liberzon_calculus_2012}. 
Initially the problem of optimal control consists of finding an optimal trajectory within a space of functions. 
PMP transforms this infinite dimensional problem into a finite dimensional boundary value problem and a maximum condition on a function defined by the cost function (Hamiltonian).
PMP plays a key role in many applications in many disciplines such as biology, physics, finance, engineering, and more recently machine learning \cite{ryll_optimal_2017, hartmann_optimal_2014,mohd-yusof_optimal_2008, la_torre_optimal_2015, chen_neural_2018}.

The extended system constructed via PMP has a rich geometrical structure which has allows to leverage several methods of differential geometry to obtain exact reductions of the original system. 
Some examples of reduction procedures are:
the use of the language of vector bundles and their symmetries to obtain exact reductions of control systems \cite{pappas_consistent_2002},
the use of tools from Poisson geometry to obtain a exact reduction of PMP \cite{ohsawa_symmetry_2013}, 
and the study of reduction of PMP using category theory \cite{borovikov_order_2004}. 
While in theory, these mathematically inclined approaches work, in practice due to the size of the systems being studied it becomes necessary to find algorithms that can automatically carry out reduction while preserving optimality. 
Other works such as \cite{daraghmeh_model_2016,petersson_nonlinear_2013} have come up with methodologies that preserve optimality using approximation methods instead of exact reduction methods. 
Using tools from theoretical computer science, works such as \cite{tabuada_bisimilar_2004, pappas_bisimilar_2003} study reduction procedures. 
Although all of the aforementioned procedures are conceptually sound, they still involve a large amount of manual work from the modeler's part, which is time consuming and more error prone than an automated algorithm.
It should be noted that concrete algorithms (as in the spirit of \cite{ovchinnikov_clue_2021, cardelli_exact_2021} have not been reported. 
The biggest challenge to use these results is to translate these mathematical formalisms into concrete algorithms that could be used by professionals without a strong background in theoretical mathematics.

Works such as \cite{boreale_algorithms_2018, boreale_algebra_2017} use algebraic and computer science tools to obtain invariants for polynomial dynamical systems.
Although these works do have a concrete implementation, they rely on the use of Buchberger's algorithm which has exponential complexity \cite[]{basu_algorithms_2010}.
This means that, in practice, this algorithm is only efficient for small systems.
It can also be possible to relate known mathematical notions of reduction to that of bisimulation \cite[]{van_der_schaft_equivalence_2004,van_der_schaft_bisimulation_2004}. 
This approach has lead to an efficient algorithm for the reduction of linear systems, which can be generalized to the nonlinear case. 
Nonetheless, this extension to the nonlinear case relies on computations on manifolds. 
These computations can be very difficult to carry out in practice.
Note that the algorithms to be studied and proposed in this project should be efficient, that being having polynomial time complexity.

We would like to close this section by mentioning some of the possible applications of optimality preserving reduction methods:
\begin{itemize}
    \item Control theory: PMP has been used in control theory for many applications throughout control theory. 
        We would like to mention the study of reachability and observability as in \cite{doncel_utopic_2019,cardelli_guaranteed_2018}.
    \item Machine learning:
        in recent years, thanks to the availability of large amounts of data, there has been a boon in research in machine learning. 
        Inside the field of deep learning, one method is that of ODE networks.
        Instead of having a discrete sequence of layers, ODE networks define a continuous transformation of the state. 
        Here, PMP appears as a generalization of back-propagation to ODE networks \cite{chen_neural_2018} .
    \item Model predictive control (MPC): the systems to be solved model large real-life systems. 
        These lead to systems of nonlinear ODEs with thousands of parameters. 
        As this control is done online, the speed of computation is key for the proper function of the MPC. 
        Optimality preserving reduction methods can lead to more efficient computations while preserving intricate interactions inside the system \cite{lohning_model_2014, lorenzetti_linear_2020}.
		More generally, we are interested in the reduction of PMP as it is a necessary condition for optimal control. 
		In this sense, any reduction that preserves optimal control must preserve PMP \cite{bemporad_explicit_2002, liberzon_calculus_2012} .
\end{itemize}
The study of optimality preserving reduction has also industrial applications.
These come particularly from applications that require high dimensional models such as:
\begin{itemize}
	\item Chemical reaction networks: these are models that are composed by large systems of ODEs.
		They are widely used in systems biology to study topics such as metabolic networks. 
		But they are also used in engineering applications such as oil refining and (bio) chemical reactor design \cite{ovchinnikov_clue_2021, cardelli_exact_2021}.
    \item Fluid dynamics: here the models used are PDEs which, in order to compute a solution, lead to discretized systems thousands and even millions of parameters \cite{nelsen_reduced_2018}.
\end{itemize}

\subsection{Project objectives}
The general goal of the project is to propose conceptually sound reduction algorithms that preserve optimality.
Specifically, the objectives of the project can be stated as:
\begin{itemize}
    \item Propose a computable criteria to verify that an algorithm preserves optimality.
    \item Write reduction algorithms that are guaranteed to preserve optimality.
    \item Demonstrate the relevance of the techniques developed by applying them to case studies in the study of biological or cyber-physical systems. 
\end{itemize}
\subsection{Key methods}
\begin{itemize}
    \item Theoretical methods:
        \begin{itemize}
            \item Pontriagyn's Maximum Principle
            \item Geometrical methods of reduction (i.e. Marsden-Weinstein-Meyer reduction).
            \item Methods of reduction (i.e. lumping, time-scale separation).
        \end{itemize}
    \item Implementation methods:
        \begin{itemize}
            \item Algebraic methods for computation (i.e. using finite fields).
            \item Known reduction algorithms (i.e. lumping, time-scale separation).
        \end{itemize}
\end{itemize}

\subsection{Significance and outcome}
This PhD study will develop methods to verify that an algorithm preserves optimality in the sense of Pontriagyn's principle.
These new techniques will be available either as a standalone tool or as a part of existing tools to be used by other scientists.


\section{Work and publication plans}

The works is divided in three parts. 
First, we will study under which conditions the reduction tool CLUE \cite{ovchinnikov_clue_2021}  preserves optimality.
The next step consists of proposing efficient algorithms to identify optimality preserving conditions.
Moreover, our next goal is to create algorithms based on the procedures using theoretical mathematics such as differential geometry, (Lie) group symmetries and abstract algebra among others.
The final part of the project will cover applications.
We aim to implement a tool that uses previous results, as well as apply it to problems in systems biology or model predictive control.
Each of these parts is expected to take around a year. 
However, it should be noted that the second part is the more theoretically demanding. 
Keeping this in mind, it might be possible to reduce the time allocated to the applications of the results.
 
Most of coursework, teaching assistance, and project supervision duties is scheduled to be achieved during the first two years. 
This in order to focus on the external stay and the thesis during the last year.


\subsection{Work and time plans}
\begin{itemize}
    \item Spring 2022
        \begin{itemize}
            \item Courses: 9.5 ECTS + 4 ECTS Summer School
            \item Supervision: 200 hours
        \end{itemize}
    \item Fall 2022 
        \begin{itemize}
            \item Courses: 6 ECTS + 4 ECTS Summer School
            \item Supervision: 200 hours
            \item \textbf{Milestone:} a paper on CLUE and optimal control.
        \end{itemize}

    \item Spring 2023
        \begin{itemize}
            \item Courses: 8.5 ECTS
            \item Supervision: 200 hours
        \end{itemize}
    \item Fall 2023 
        \begin{itemize}
            \item Supervision: 150 hours
            \item \textbf{Milestone:} a conference paper on general lumping algorithms and optimal control.
            \item \textbf{Milestone:} a paper describing the implementation of optimality-preserving lumping algorithms.
        \end{itemize}

    \item Spring 2024
        \begin{itemize}
            \item External stay.
			\item \textbf{Milestone:} a journal paper extending foregoing research.
        \end{itemize}
    \item Fall 2024 
        \begin{itemize}
            \item \textbf{Milestone:} a paper on optimality-preserving reduction and model predictive control.
            \item \textbf{Milestone:} completion of the PhD thesis.
        \end{itemize}
\end{itemize}

\subsection{Outline of the thesis}

The PhD thesis will take the form of a collection of papers outlined in subsection \ref{ssec:tpl} together with an extended summary and outlook. 
In the thesis, there will be a overview of the state-of-the-art tied to a motivation for the project. 
Next, the contributions of each of the papers will be highlighted and will be put into context.
Finally, I will write the conclusions of the PhD work as well as an overview of possible future research directions. 


\subsection{Tentative publication list} \label{ssec:tpl}

The following is a list of possible co-authors:
\href{https://lucacardelli.name/index.html}{Luca Cardelli},
\href{http://people.cs.aau.dk/~kgl/}{Kim G. Larsen}, 
\href{http://www.imm.dtu.dk/~albl/}{Prof. Alberto Lluch-Lafuente}, 
\href{https://sysma.imtlucca.it/pages/mirco-tribastone/}{Mirco Tribastone},
\href{https://maxtschaikowski.com/}{Max Tschaikowski},
\href{http://people.compute.dtu.dk/anvan/}{Andrea Vandin}.


The following is a tentative list of publications, each publication might include one or more of the aforementioned co-authors.
\begin{itemize}
    \item CLUE as an optimality preserving algorithm.
	\item Theoretical conference paper on lumping and optimality.
    \item Tool paper describing the implementation of previous results.
    \item Journal paper extending foregoing results.
	\item \textbf{(Optional)} Conference paper on the applications of this research to MPC.
\end{itemize}

\section{Supervision/student co-operation agreements}

This PhD project is supervised by Max Tschaikowski, who has a strong background on model reduction, optimal control, and formal methods.
We schedule weekly meetings to evaluate the progress of the project. \\
The professor \href{http://people.compute.dtu.dk/anvan/}{Andrea Vandin} at DTU will be co-supervisor for this PhD. 
We schedule meetings and possible visits every six months.
\section{Plan for PhD courses}

\begin{table}[h]
	\begin{center}
		\caption{List of courses.}
		\begin{tabular}{|p{7cm} | c | c | c | c|}
			\hline
			Courses & Organizer & ECTS & Type & Start \\ [0.5ex] 
			\hline\hline
			Introduction to the PhD study & AAU & 0.5 & G & Spring 2022\\
			Danish code of conduct & AAU & 1 & G & Spring 2022\\
			Academic writing in English & AAU & 2.5 & G & Spring 2022\\
			Nonlinear differential equations and dynamical systems & AAU & 3 & P& \\
			Scientific Computing Using Python 1 & AAU & 2.5 & G & Spring 2022\\
			Relevant Summer School & & 4 &P & Summer 2022 \\

			Intellectual property rights & AAU & 2 & G & Fall 2022\\
			Time-series modelling and analytics & AAU & 2 & P & Fall 2022\\
			Scientific Computing Using Python 2 & AAU & 2 & G & Fall 2022\\
			Advanced Program Verification & AAU & 2 & P& Fall 2022 \\
			Signal and spectral analysis& AAU & 2 & G & Fall 2022 \\

			From research to business & AAU & 2 & G& Spring 2023 \\
			Stakeholder and user-involvement in technological innovation and implementation & AAU & 2.5 & G & Spring 2023\\
			Relevant Summer School & &4 &P & Summer 2023
			\\ [0.5ex] 
			\hline\hline
			Total General Courses & \multicolumn{4}{c|}{17} \\  
			Total Project Courses & \multicolumn{4}{c|}{15} \\  
			Total Courses &\multicolumn{4}{c|}{32} \\  
			\hline
		\end{tabular}
	\end{center}
\end{table}


\newpage


\section{Plan for fulfillment of knowledge dissemination}

We aim to present at the following high ranked venues (listed alongside their CORE ranking and BFI level):

\begin{table}[h]
	\centering
	\caption{Venues in theoretical computer science}
	%\label{tab:label}
	\begin{tabular}{c p{7cm} c c}
		& & CORE 2021 & BFI 2021  \\ \hline
		TACAS & International Conference on Tools and Algorithms for the Construction and Analysis of Systems. & A & 2\\
		FoSSaCS & International Conference on Foundations of Software Science and Computation Structures.& A & 2\\
		ATVA & Internaional Symposium on Automated Technology for Verification and Analysis. & B & 2 \\
		CAV & International Conference on Computer-Aided Verification. & A* & 2\\

		CONCUR & International Conference on Concurrency Theory. & A& 2 \\
		QEST & International Conference on Quantitative Evaluation of SysTems. & - & 1 \\ 
		\hline
	\end{tabular}
\end{table}

For venues on control engineering we list only the BFI ranking:

\begin{table}[h]
	\centering
	\caption{Venues in control engineering.}
	%\label{tab:label}
	\begin{tabular}{c p{7cm} c c}
		& &  BFI 2021  \\ \hline
		CDC & IEEE Conference on Decision and Control. & 1\\
		EEC	& European Control Conference. & 1\\
		HSCC & International Conference on Hybrid Systems: Computation and Control. & 1\\
		DYCOPS-CAB & IFAC Symposium on Dynamics and Control of Process Systems, including Biosystems
				   & 1 \\
		\hline
	\end{tabular}
\end{table}

Table \ref{tab:sup} shows how the 750 teaching hours are going to be met throughout the PhD.
These hours have to be spent specifically on project supervision in agreement with Aalborg University.
The plan shown in Table \ref{tab:sup} is tentative and it is subject to changes depending on available supervision and teaching assistance options.

\begin{table}[h!]
\begin{center}
\caption{Plan of supervision.}
\label{tab:sup}
    \begin{tabular}{| c | c | c |} 
 \hline
 Semester & Description& Hours \\ [0.5ex] 
 \hline\hline
 Spring 2022 & 2xP Supervision & 200 \\
 Fall 2022 & 2xP Supervision & 200 \\
 Spring 2023 & 2xP Supervision & 200 \\
 Fall 2023 & 2xP Supervision & 130 \\
           & Censoring & 20
\\ [0.5ex] 
 \hline\hline
 Total &  & 750 \\  
 \hline
\end{tabular}
\end{center}
\end{table}

\section{Agreements on immaterial rights to patents}

The intellectual property right follows the standard Aalborg University regulations unless stated otherwise in mutual agreement between both parties (AAU and me as a student). 
Any developed tools will be distributed under the GNU Lesser General Public License (LGPL) v3 which ensures that any derivative work can only be redistributed under the same license.
Extensions of existing tools or software will be developed under a compatible license, with a strong leaning towards open licensing.

\section{External co-operation}

At this point we consider two options for an external stay of up to 4 months:
\begin{itemize}
    \item \href{http://lucacardelli.name/}{Prof. Luca Cardelli} at the \href{https://www.cs.ox.ac.uk/}{Department of Computer Science} at Oxford University. 
	\item \href{http://www.imm.dtu.dk/~albl/}{Prof. Alberto Lluch-Lafuente} at the \href{https://www.compute.dtu.dk/english}{Department of Applied Mathematics and Computer Science} at the Technical University of Denmark (DTU). 
    \item \href{http://cse.lab.imtlucca.it/~mirco.tribastone/#bio}{Prof. Mirco Tribastone} at the \href{https://sysma.imtlucca.it}{SySMA unit} at the IMT Luca.
\end{itemize}

%\section{Career Plan}

\section{Financing budget}

The PhD project is financed   $(1/3)$ by the Department of Computer Science at Aalborg Universityand $(2/3)$ by the Poul Due Jensen Foundation grant number $883901$.

%------------------
% BIBLIOGRAPHY
%------------------

\bibliographystyle{ieeetr} %Estilo bibliografía.
\bibliography{PhD.bib} %Cite el archivo que contiene la bibliografía. Debe ser archivo .bib. Se edita con TexWorks, por ejemplo.

%------------------
% END OF THE DOCUMENT
%------------------

\end{document}
